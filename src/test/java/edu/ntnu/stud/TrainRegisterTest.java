package edu.ntnu.stud;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class TrainRegisterTest {
  //Frequently used variables.
  private final TrainRegister trainRegister = new TrainRegister();
  private TrainDeparture train1;
  private TrainDeparture train2;
  private TrainDeparture train3;
  private TrainDeparture train4;
  private TrainDeparture train5;
  private TrainDeparture train6;

  private final LocalTime departureTime = LocalTime.parse("10:00");
  private final String line = "L1";
  private final String destination = "Oslo";
  private final int track = 1;


  @BeforeEach
  @DisplayName("Set current time of the day, and pre-initialize train departures to use in the TrainRegisterTest-class.")
  void setUp() {

    train6 = trainRegister.getNewTrainRegistered(LocalTime.parse(
        "09:10"), "X3", 11, "Oslo", 4);

    train5 = trainRegister.getNewTrainRegistered(
        LocalTime.parse("09:00"), "X10", 10, "Foss", 5);

    //sets a new current time after registering two train with departure time before current time.
    trainRegister.setCurrentTime(LocalTime.parse("10:00"));

    train1 = trainRegister.getNewTrainRegistered(
        LocalTime.parse("10:10"), "L1", 1, "Lillestrøm", 1);

    train2 = trainRegister.getNewTrainRegistered(
        LocalTime.parse("10:10"), "R12", 2, "Kongsberg", 2);

    train3 = trainRegister.getNewTrainRegistered(
        LocalTime.parse("10:20"), "R13", 3, "Drammen", 3);

    train4 = trainRegister.getNewTrainRegistered(
        LocalTime.parse("10:30"), "L2", 4, "Stabekk", 4);

  }


  @Nested
  @DisplayName("Positive tests for Train Register Class.")
  class PositiveTests {
    @Test
    @DisplayName("TrainRegister creates and add train departure with different train number in the register.")
    void testAddUnusedTrainNumber() {
      assertDoesNotThrow(() -> trainRegister.getNewTrainRegistered(departureTime, line, 5, destination, track));

    }


    @Test
    @DisplayName("TrainRegister returns train departures with train number equal to valid train number input.")
    void testGetTrainDepByValidTrainNumber() {
      TrainDeparture getTrain2 = trainRegister.getTrainDepByTrainNumber(2);
      TrainDeparture getTrain3 = trainRegister.getTrainDepByTrainNumber(3);
      TrainDeparture getTrain5 = trainRegister.getTrainDepByTrainNumber(10);

      assertEquals(train2, getTrain2);
      assertEquals(train3, getTrain3);
      assertEquals(train5, getTrain5);
    }

    @Test
    @DisplayName("TrainRegister returns null if train departure does not exist, caused by a non-existing train number from input.")
    void testGetNullByTrainNumberNonExisting() {
      TrainDeparture getNonExistingTrain = trainRegister.getTrainDepByTrainNumber(6);
      assertNull(getNonExistingTrain);
    }


    @Test
    @DisplayName("TrainRegister returns train departure(s) with destination equal to valid destination input.")
    void testGetTrainDepByValidDestination() {
      //parameter gets compared with lower cases letters
      List<TrainDeparture> trainDeparturesToLillestrom = trainRegister.getTrainDepByDestination("lillestrøm");
      List<TrainDeparture> trainDeparturesToFoss = trainRegister.getTrainDepByDestination("fOss");

      assertEquals(1, trainDeparturesToLillestrom.size());
      assertEquals(1, trainDeparturesToFoss.size());
    }

    @Test
    @DisplayName("TrainRegister returns future train departures with track equal to valid track input.")
    void testGetTrainDepByValidTrack() {
      List<TrainDeparture> trainDeparturesFromTrack1 = trainRegister.getFutureTrainDepByTrack(1);
      List<TrainDeparture> trainDeparturesFromTrack2 = trainRegister.getFutureTrainDepByTrack(2);

      assertEquals(1, trainDeparturesFromTrack1.size());
      assertEquals(1, trainDeparturesFromTrack2.size());
    }



    @Test
    @DisplayName("TrainRegister returns empty list if no train departures departs from the valid track from input.")
    void testGetTrainDepByNonUsedTrack() {
      List<TrainDeparture> trainDeparturesFromNonUsedTrack = trainRegister.getFutureTrainDepByTrack(10);

      assertTrue(trainDeparturesFromNonUsedTrack.isEmpty());
    }


    @Test
    @DisplayName("TrainRegister returns train departures within valid time interval from user input.")
    void testGetTrainDepTimeInterval() {
      LocalTime fromTime = LocalTime.parse("10:10");
      LocalTime toTime = LocalTime.parse("12:00");

      List<TrainDeparture> trainDeparturesWithinInterval = trainRegister.getTrainDepWithinTimeInterval(fromTime, toTime);

      assertEquals(4, trainDeparturesWithinInterval.size());
    }

    @Test
    @DisplayName("TrainRegister collects and sorts future train departures with departure from same track.")
    void testGetTrainDepTimeIntervalSorted() {
      LocalTime fromTime = LocalTime.parse("10:10");
      LocalTime toTime = LocalTime.parse("12:00");

      List<TrainDeparture> withinInterval = trainRegister.getTrainDepTimeIntervalSorted(fromTime, toTime);

      //Verify sorted
      boolean isSorted = IntStream.range(0, withinInterval.size() - 1)
                             .allMatch(trainDep -> withinInterval.get(trainDep).getDepartureTime()
                                                       .isBefore(withinInterval.get(trainDep + 1).getDepartureTime()) ||
                                                       withinInterval.get(trainDep).getDepartureTime()
                                                           .equals(withinInterval.get(trainDep + 1).getDepartureTime()));

      assertTrue(isSorted);


    }

    @Test
    @DisplayName("TrainRegister updates the current time when provided with a valid time input for the new current time set by the user.")
    void testSetNewValidCurrentTime() {
      LocalTime newCurrentTime = LocalTime.parse("10:10");

      trainRegister.setCurrentTime(newCurrentTime);

      assertEquals(newCurrentTime, trainRegister.getCurrentTime());
    }

    @Test
    @DisplayName("TrainRegister applies delay to a train departure, not changing the original departure time.")
    void testGetTrainDepWithValidNewDelay() {
      TrainDeparture delayedTrain1 = trainRegister.getTrainDepWithNewDelay(train1, LocalTime.parse("00:30"));

      assertEquals(LocalTime.parse("00:30"), delayedTrain1.getDelay());
      //Assures that original departure time is unchanged.
      assertEquals(LocalTime.parse("10:10"), delayedTrain1.getDepartureTime());
    }

    @Test
    @DisplayName("TrainRegister calculates departure time added by delay, not changing the original departure time.")
    void testCalculateDepWithDelay() {
      LocalTime delay = LocalTime.parse("00:30");

      LocalTime depWithDelay = trainRegister.depWithDelay(train1.getDepartureTime(), delay);

      assertEquals(LocalTime.parse("00:30"), delay);
      assertEquals(LocalTime.parse("10:10"), train1.getDepartureTime());
      assertEquals(LocalTime.parse("10:40"), depWithDelay);
    }


    @Test
    @DisplayName("TrainRegister returns true if changing track does not conflict with other trains on the same track.")
    void testIsNotWithin10MinutesTrue() {

      //Case: changing track on an existing train
      boolean result = trainRegister.has10MinGapSameTrack(train1, 3);
      assertTrue(result);

      //Case: assigning same track on an existing train
      boolean result2 = trainRegister.has10MinGapSameTrack(train1, 1);
      assertTrue(result2);

      //Case: assigning track to a new train
      TrainDeparture newTrainDeparture =
          trainRegister.getNewTrainRegistered(LocalTime.parse("10:20"), line, 19, destination, -1);

      boolean result3 = trainRegister.has10MinGapSameTrack(newTrainDeparture, 1);
      assertTrue(result3);
    }

    @Test
    @DisplayName("TrainRegister does not throw exception when the new track does not conflict with other train departures on the same track.")
    void testVerifyNotWithin10MinutesNotThrow() {

      //Case: assigning same track
      assertDoesNotThrow(() -> trainRegister.verifyTrackForTrainDeparture(train1, 1));

      //Case: assigning track to an existing train departure
      assertDoesNotThrow(() -> trainRegister.verifyTrackForTrainDeparture(train1, 3));

      //Case: assigning track to a new train
      TrainDeparture newTrainDeparture =
          trainRegister.getNewTrainRegistered(LocalTime.parse("10:20"), line, 19, destination, -1);

      assertDoesNotThrow(() -> trainRegister.verifyTrackForTrainDeparture(newTrainDeparture, 1));
    }

    @Test
    @DisplayName("TrainRegister does not throw exception if the assigned track does not conflict with other train departures on the same track.")
    void testGetTrainDepWithValidNewTrack() {

      //Case: assigning same track
      assertDoesNotThrow(() -> trainRegister.getTrainDepWithNewTrack(train1, 1));

      //Case: assigning track to an existing train departure
      assertDoesNotThrow(() -> trainRegister.getTrainDepWithNewTrack(train1, 3));

      //Case: assigning track to a new train
      TrainDeparture newTrainDeparture =
          trainRegister.getNewTrainRegistered(LocalTime.parse("10:20"), line, 19, destination, -1);

      assertDoesNotThrow(() -> trainRegister.getTrainDepWithNewTrack(newTrainDeparture, 1));
    }

    @Test
    @DisplayName("TrainRegister sorts a list by original departure time.")
    void testGetSortedByTime() {
      List<TrainDeparture> unsortedList = List.of(train6, train4, train2, train1, train3, train5);

      List<TrainDeparture> sortedList = trainRegister.getSortedByTime(unsortedList);

      //Verify sorted
      boolean isSorted = IntStream.range(0, sortedList.size() - 1)
                             .allMatch(trainDep -> sortedList.get(trainDep).getDepartureTime()
                                                .isBefore(sortedList.get(trainDep + 1).getDepartureTime()) ||
                                                sortedList.get(trainDep).getDepartureTime()
                                                    .equals(sortedList.get(trainDep + 1).getDepartureTime()));

      assertTrue(isSorted);
    }


    @Test
    @DisplayName("TrainRegister returns a list of train departures with actual departure time equal, or after, the current time.")
    void testGetFutureDepartures() {
      trainRegister.setCurrentTime(LocalTime.parse("10:30"));

      List<TrainDeparture> futureDepartures = trainRegister.getFutureDepartures();

      assertFalse(futureDepartures.contains(train1));
      assertFalse(futureDepartures.contains(train2));
      assertFalse(futureDepartures.contains(train3));
      assertFalse(futureDepartures.contains(train5));
      assertFalse(futureDepartures.contains(train6));
      assertFalse(futureDepartures.isEmpty());
    }


    @Test
    @DisplayName("TrainRegister collects and sorts future departures by original departure time.")
    void testGetFutureDeparturesSorted() {
      trainRegister.setCurrentTime(LocalTime.parse("10:11"));

      List<TrainDeparture> sortedList = trainRegister.getFutureDeparturesSorted();

      //Verify sorted
      boolean isSorted = IntStream.range(0, sortedList.size() - 1)
                             .allMatch(trainDep -> sortedList.get(trainDep).getDepartureTime()
                                                       .isBefore(sortedList.get(trainDep + 1).getDepartureTime()) ||
                                                       sortedList.get(trainDep).getDepartureTime()
                                                           .equals(sortedList.get(trainDep + 1).getDepartureTime()));

      assertTrue(isSorted);
    }

    @Test
    @DisplayName("TrainRegister does not throw exception when an existing train departure is chosen to delete.")
    void testDeleteExistingTrainDeparture() {

      assertDoesNotThrow(() -> trainRegister.deleteTrainDep(train1));
    }

    @Test
    @DisplayName("TrainRegister does not return train departures that has already departed from specific valid track from input.")
    void testGetDepartedTrainDepByTrack() {
      List<TrainDeparture> trainDeparturesFromTrack5 = trainRegister.getFutureTrainDepByTrack(5);
      List<TrainDeparture> trainDeparturesFromTrack4 = trainRegister.getFutureTrainDepByTrack(4);

      assertTrue(trainDeparturesFromTrack5.isEmpty());
      assertEquals(1, trainDeparturesFromTrack4.size());
    }
  }





  @Nested
  @DisplayName("Negative tests for TrainRegister Class.")
  class NegativeTests {
    @Test
    @DisplayName("TrainRegister throws exception when attempted to add train with existing train number.")
    void testAddExistingTrainNumber() {
      assertThrows(IllegalArgumentException.class, () ->trainRegister.getNewTrainRegistered(
          departureTime, line, 1, destination, track));
    }

    @Test
    @DisplayName("TrainRegister throws exception whenn attempted to add train departure that break the 10-minute-gap rule")
    void testAddTrainNot10MinuteGap() {
      assertThrows(IllegalArgumentException.class, () ->trainRegister.getNewTrainRegistered(
          LocalTime.parse("10:10"), line, 17, destination, 1));
    }

    @Test
    @DisplayName("TrainRegister throws exception if the new current time from user input is before the current time.")
    void testSetNewCurrentTimeBefore() {
      assertThrows(IllegalArgumentException.class, () -> trainRegister.setCurrentTime(LocalTime.parse("09:00")));
    }

    @Test
    @DisplayName("TrainRegister throws exception if the new current time from user input is the same as the current time.")
    void testSetNewCurrentTimeSame() {
      assertThrows(IllegalArgumentException.class, () -> trainRegister.setCurrentTime(LocalTime.parse("10:00")));
    }


    @Test
    @DisplayName("TrainRegister returns false if changing track conflicts with other trains departures on the same track.")
    void testIsNotWithin10MinutesFalse() {

      //Case: changing track on an existing train
      boolean result = trainRegister.has10MinGapSameTrack(train2, 1);
      assertFalse(result);

      //Case: assigning track on a new train
      TrainDeparture newTrainDeparture =
          trainRegister.getNewTrainRegistered(LocalTime.parse("10:10"), line, 444, destination, -1);

      boolean result2 = trainRegister.has10MinGapSameTrack(newTrainDeparture, 1);
      assertFalse(result2);
    }

    @Test
    @DisplayName("TrainRegister throws exception when verifying if the new track creates " +
                     "conflicts between train departures on the same track.")
    void testVerifyNotWithin10MinutesThrow() {

      //Case: changing track on an existing train
      assertThrows(IllegalArgumentException.class,
          () -> trainRegister.verifyTrackForTrainDeparture(train2, 1));

      //Case: assigning track on a new train
      TrainDeparture newTrainDeparture =
          trainRegister.getNewTrainRegistered(LocalTime.parse("10:05"), line, 444, destination, -1);

      assertThrows(IllegalArgumentException.class,
          () -> trainRegister.verifyTrackForTrainDeparture(newTrainDeparture, 2));

    }

    @Test
    @DisplayName("TrainRegister throws exception when verifying if a new track assigning creates " +
                     "conflicts between train departures on the same track.")
    void testGetTrainDepWithConflictingNewTrack() {

      //Case: changing track on an existing train
      assertThrows(IllegalArgumentException.class,
          () -> trainRegister.getTrainDepWithNewTrack(train2, 1));

      //Case: assigning track on a new train
      TrainDeparture newTrainDeparture =
          trainRegister.getNewTrainRegistered(LocalTime.parse("10:05"), line, 444, destination, -1);

      assertThrows(IllegalArgumentException.class,
          () -> trainRegister.getTrainDepWithNewTrack(newTrainDeparture, 2));
    }

    @Test
    @DisplayName("TrainRegister throws exception when non-existing train in the register is chosen to be deleted.")
    void testDeleteNonExistingTrainDep() {
      //Creates a new train departure without adding it to the register.
      TrainDeparture trainDepartureNotInRegister = new TrainDeparture(departureTime, line, 999, destination, track);

      assertThrows(IllegalArgumentException.class, () -> trainRegister.deleteTrainDep(trainDepartureNotInRegister));
    }

  }

}














