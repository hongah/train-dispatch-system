package edu.ntnu.stud;

import edu.ntnu.stud.model.TrainDeparture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.LocalTime;
import static org.junit.jupiter.api.Assertions.*;
import java.time.format.DateTimeParseException;

public class TrainDepartureTest {
  //Frequently used variables.
  private TrainDeparture trainDeparture;
  private final LocalTime departureTime = LocalTime.parse("10:00");
  private final String line = "L1";
  private final int trainNumber = 1;
  private final String destination = "Oslo";
  private final int track = 1;
  private final int track2 = 2;
  private final LocalTime setValidDelay = LocalTime.parse("00:30");


  @BeforeEach
  @DisplayName("Creates a new train departure.")
  void setUp() {
    trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track);
  }


  @Nested
  @DisplayName("Positive tests for TrainDeparture Class.")
  class PositiveTests {
    @Test
    @DisplayName("TrainDeparture creates a train departure without throwing exception.")
    void testValidTrainDeparture() {
      assertDoesNotThrow(() -> new TrainDeparture(departureTime, line, trainNumber, destination, track));
    }

    @Test
    @DisplayName("TrainDeparture creates a train departure with no assigned track, without throwing exception.")
    void testNoAssignedTrack() {
      assertDoesNotThrow(() -> new TrainDeparture(departureTime, line, trainNumber, destination, -1));
    }

    @Test
    @DisplayName("TrainDeparture sets a valid track on existing train departure, without throwing exception.")
    void testSetValidTrack() {
      trainDeparture.setTrack(track2);
      assertEquals(track2, trainDeparture.getTrack());
    }

    @Test
    @DisplayName("Train Departure modifies the delay on a existing train departure.")
    void testSetValidDelay() {
      trainDeparture.setDelay(setValidDelay);
      assertEquals((setValidDelay), trainDeparture.getDelay());
    }

    @Test
    @DisplayName("Train Departure sets a valid delay in format HH:MM:SS on existing train departure, without throwing exception.")
    void testSetValidDelayDiffFormat() {
      LocalTime delay = LocalTime.parse("10:10:00");
      assertDoesNotThrow(() -> trainDeparture.setDelay(delay));

    }

    @Test
    @DisplayName("Equals method returns false on two train departures with different train numbers.")
    void testNotEquals() {
      TrainDeparture notSame = new TrainDeparture(departureTime, line, 2, destination, track);

      assertNotEquals(trainDeparture, notSame);
    }

    @Test
    @DisplayName("Equals method returns true on two train departures with same train numbers.")
    void testEquals() {
      //Different versions of train departures with same train number
      TrainDeparture sameCanOne = new TrainDeparture(departureTime, line, trainNumber, destination, track);
      TrainDeparture sameCanTwo = new TrainDeparture(departureTime, "X10", trainNumber, "destination", track2);

      assertEquals(trainDeparture, sameCanOne);
      assertEquals(trainDeparture, sameCanTwo);
    }

    @Test
    @DisplayName("Header to be printed in expected format.")
    void testGetHeader() {
      String expected = "Departure time   Line  Train number   Destination    Delay   Track";

      assertEquals(expected, TrainDeparture.getHeader());
    }

    @Test
    @DisplayName("Train departure to be printed in expected format.")
    void testToString() {
      String expected = "10:00            L1    1              Oslo                   1    ";

      assertEquals(expected, trainDeparture.toString());
    }

    @Test
    @DisplayName("TrainDeparture does not throw exception when string parameter is not empty")
    void testStringParameterEmpty() {
      assertDoesNotThrow(() ->trainDeparture.verifyStringParameterNotEmpty("Test", "Test"));
      assertDoesNotThrow(() ->trainDeparture.verifyStringParameterNotEmpty("T e s t", "Test"));
    }

    @Test
    @DisplayName("TrainDeparture does not throw exception when time is in valid format, or seconds in LocalTime variable is equal to 0.")
    void testValidTime() {
      assertDoesNotThrow(() -> new TrainDeparture(LocalTime.parse("10:10:00"), line, trainNumber, destination, track));
    }
  }




  @Nested
  @DisplayName("Negative Tests for TrainDeparture Class. Should throw exceptions on invalid input.")
  class NegativeTests {
    @Test
    @DisplayName("TrainDeparture constructor throws exception on invalid departure time input.")
    void testInvalidDepartureTime() {
      assertThrows(DateTimeParseException.class, () -> new TrainDeparture(
          LocalTime.parse("25:00"), line, trainNumber, destination, track));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on invalid line input.")
    void testInvalidLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, "InvalidLine", trainNumber, destination, track));
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, "", trainNumber, destination, track));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on too high train number input.")
    void testTooHighTrainNumber() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, 1001, destination, track));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on too low train number input.")
    void testTooLowTrainNumber() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, -10, destination, track));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on invalid destination input.")
    void testInvalidDestination() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, "123", track));
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, "", track));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on too high track number input.")
    void testTooHighTrack() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, destination, 25));
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, destination, 100));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on too low track number input.")
    void testTooLowTrack() {
      //program should be able to set track as -1.
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, destination, -2));
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, destination, 0));
    }

    @Test
    @DisplayName("TrainDeparture constructor throws exception on blank destination input.")
    void testBlankDestination() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, line, trainNumber, "", track));
    }

    @Test
    @DisplayName("Train Departure constructor throws exception on blank line input.")
    void testBlankLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(
          departureTime, "", trainNumber, destination, track));
    }

    @Test
    @DisplayName("Train Departure throws exception on setting invalid track number.")
    void testSetInvalidTrack() {
      //program should be able to set track as -1.
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(25));
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(0));
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(-2));
    }

    @Test
    @DisplayName("Train Departure throws exception on setting delay higher than a 24-hour range.")
    void testSetHigherThan24HoursDelay() {
      assertThrows(DateTimeParseException.class, () -> trainDeparture.setDelay(LocalTime.parse("25:00")));
    }

    @Test
    @DisplayName("Train Departure throws exception on setting a negative delay.")
    void testSetNegativeDelay() {
      assertThrows(DateTimeParseException.class, () -> trainDeparture.setDelay(LocalTime.parse("-01:00")));
    }

    @Test
    @DisplayName("TrainDeparture throws exception when string parameter is empty")
    void testStringParameterEmpty() {
      assertThrows(IllegalArgumentException.class, () ->trainDeparture.verifyStringParameterNotEmpty(" ", "Test"));
      assertThrows(IllegalArgumentException.class, () ->trainDeparture.verifyStringParameterNotEmpty("", "Test"));
    }

    @Test
    @DisplayName("TrainDeparture throws exception when departure time is in invalid format, "
                     + "or a format defined with seconds not equal to 0")
    void testNotValidTime() {
      assertThrows(IllegalArgumentException.class, () ->trainDeparture.verifyFormatTimeParameter(LocalTime.parse("12:00:10"), "Test"));
    }
  }


}

