package edu.ntnu.stud.view;

import java.time.LocalTime;

/**
 * Menu class.
 * Prints out all menus that are shown the application.
 * Goal: Print menu for the application to display for the user.
 *
 * @author Hong An Ho
 * @version 1.2
 * @since 1.1
 */
public class Menu {


  /**
   * Constructs a new Menu instance.
   * Creates a new object of Menu without requiring any attribute initialization or set up.
   */
  public Menu() {
    //No initialization of attributes required to use the Menu class.
  }


  /**
   * Displays a menu asking the user to choose between assigning a track or not.
   */
  public void showTrackMenu() {
    System.out.println(" "); //empty line
    System.out.println("Do you wish to assign a track now?");
    System.out.println("1| Yes, assign track.");
    System.out.println("2| No, not yet.");
    System.out.println(" "); //empty line
    System.out.println("Please enter a number from 1-2.");
  }


  /**
   * Displays the main menu options for the operator.
   * The main menu presents a list of operations the operator can do on the station.
   * Prints a guiding message to enter an option from 1 to 12.
   *
   * @param currentTime is a LocalTime representing the current time of the day.
   */
  public void showMainMenu(LocalTime currentTime) {
    System.out.println(" "); //empty line
    System.out.println("---------------------MAIN MENU FOR OPERATOR---------------------");
    System.out.println("                Current time of the day: " + currentTime);
    System.out.println(" "); //empty line
    System.out.println(" 1 |  Information table.");
    System.out.println(" 2 |  Update clock.");
    System.out.println(" 3 |  Add new train departure.");
    System.out.println(" 4 |  Delete a train departure.");
    System.out.println(" 5 |  Assign or change track to existing train departure.");
    System.out.println(" 6 |  Set delay to a train departure.");
    System.out.println(" 7 |  Search, based on train number.");
    System.out.println(" 8 |  Search, based on destination.");
    System.out.println(" 9 |  Search, from a specific track.");
    System.out.println("10 |  Search, within a time interval.");
    System.out.println("11 |  Write out alle train departures registered for departure today.");
    System.out.println("12 |  Quit application.");

    System.out.println(" "); //empty line
    System.out.println("Please enter a number from 1-12.");
  }



  /**
   * Prints a deletion menu for conformation, before deleting a train departure.
   * Shows option to confirm the deletion, or to cancel the deletion of the given train departure.
   * Provides a reassurance step before finally deleting a departure.
   *
   * @param trainDeparturePrinted is a String showing the train departure to be confirmed
   *                              for deletion.
   */
  public void showDeletionMenu(String trainDeparturePrinted) {
    System.out.println(" ");
    System.out.println("Are you sure you want to delete this train departure? ");
    System.out.println(trainDeparturePrinted);
    System.out.println(" "); //empty line
    System.out.println("1| Yes, delete.");
    System.out.println("2| No, don't delete.");
    System.out.println(" "); //empty line
    System.out.println("Please enter a number from 1-2.");
  }
}
