package edu.ntnu.stud.view;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainRegister;
import java.time.LocalTime;
import java.util.List;



/**
 * UserInterface class.
 * Handles interactions with user of the Train Dispatch System.
 * Goal: Interpreting user inputs, displaying relevant information,
 * and executing actions based on user choices.
 *
 * @author Hong An Ho
 * @version 1.2
 * @since 0.3
 *
 */
public class UserInterface {
  private final TrainRegister trainRegister;
  private final Menu menu;
  private final Scan scan;
  private boolean quitApplication = false;


  /**
   * Constructs an object of the class UserInterface that contains a register
   * for the Train Departures, a scan instance, and a menu instance.
   */
  public UserInterface() {
    menu = new Menu();
    scan = new Scan();
    trainRegister = new TrainRegister();
  }


  /**
   * Sets the application to quit and exit.
   */
  private void quitApplication() {
    quitApplication = true;
  }


  /**
   * Adds predefined train departures to the train register.
   * Catches IllegalArgumentException and DateTimeParseException
   * if invalid data has been defined to the attributes, if the train number exist already, or if
   * a pre-registered train departure has less than a 10-minute gap from other train departures
   * the same track, and prints corresponding error messages.
   */
  private void init() {
    try {
      trainRegister.getNewTrainRegistered(LocalTime.of(10, 10), "L1", 1, "Lillestrøm",
          1);
      trainRegister.getNewTrainRegistered(LocalTime.of(10, 10), "R12", 2, "Kongsberg",
          2);
      trainRegister.getNewTrainRegistered(LocalTime.of(10, 20), "R13", 3, "Drammen",
          3);
      trainRegister.getNewTrainRegistered(LocalTime.of(10, 30), "L2", 4, "Stabekk",
          4);
    } catch (Exception e) {
      System.out.println(" ");
      System.out.println("Found invalid data in pre-registered train departures, "
                             + "and therefore one or multiple items could not be added: "
                             + e.getMessage());
    }
  }


  /**
   * Prints a message to let the user know that the application
   * will return to the main menu.
   */
  private void returnToMainMenu() {
    System.out.println("Application is returning to main menu ..");
    start();
  }


  /**
   * Prompts the user to type in the String '0' to return to the main menu after successfully
   * executing an operation. Instruction will be printed again if user types anything else than '0'.
   */
  private void typeForReturn() {
    System.out.println(" ");
    System.out.println("Type '0' to go back to the main menu:");
    try {
      String input = scan.inputNotEmptyString();
      assert input != null;
      if (input.trim().equals("0")) {
        returnToMainMenu();
      } else {
        typeForReturn();
      }
    } catch (IllegalArgumentException e) {
      System.out.println("Invalid input registered: " + e.getMessage());
      typeForReturn();
    }
  }


  /**
   * Prompts the user to choose whether to assign a track or not.
   * If chooses to assign a track (enters 1), the method will prompt the user for a track number.
   * If chooses to not assign a track (enters 2), the method will set the track number to -1.
   * If the user enters an invalid choice, the method will print an error message and continue to
   * prompt the user for a valid choice.
   *
   * @return a track number ranging from 1-20 if assigning track was selected, and -1 if not.
   */
  private int getTrackFromChoice() {
    menu.showTrackMenu();
    if (scan.confirmChoiceOne()) {
      System.out.println("Assign track selected.");
      return scan.getTrackFromInterval();
    } else {
      System.out.println("No track selected.");
      return -1;
    }
  }

  /**
   * Retrieves a non-existing train number within the train number interval.
   * If an invalid train number or an existing train number is entered, the method will print
   * an error message and continue to prompt for a non-existent train number within the interval.
   * Does not exist in the Scan Class because of the attempt to retry or return to main menu.
   *
   * @return a valid train number ranging from 1-999 that is non-existent in the train register.
   */
  private int getNonExistingTrainNumber() {
    int trainNumber = scan.getTrainNumberFromInterval();
    TrainDeparture trainDeparture = trainRegister.getTrainDepByTrainNumber(trainNumber);
    if (trainDeparture != null) {
      System.out.println("The train number already exists, "
                             + "and therefore can't be used again.");
      System.out.println("Overview over train numbers in use can be found in main menu "
                             + "-> operation number 11.");
      if (scan.choiceRetry()) {
        trainNumber = getNonExistingTrainNumber();
      } else {
        returnToMainMenu();
      }
    }
    return trainNumber;
  }


  /**
   * Retrieves an existing train departure from the register.
   * If an invalid train number or a non-existing train number is entered, the method will print
   * an error message. User can choose to try again, or return to the main menu.
   * Does not exist in the Scan Class because of the attempt to retry or return to main menu.
   *
   * @return an existing train number in the train register.
   */
  private TrainDeparture getExistingTrainDep() {
    try {
      int trainNumber = scan.getTrainNumberFromInterval();
      return trainRegister.getExistingTrainDepByTrainNumber(trainNumber);

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      System.out.println("Overview over existing train departures can be found in main menu "
                             + "-> operation number 11.");
      if (scan.choiceRetry()) {
        return getExistingTrainDep();
      } else {
        returnToMainMenu();
        return null;
      }
    }
  }


  /**
   * Retrieves an existing train departure from train number, that has actual departure time equal,
   * or after, the current time. The method will continue to prompt until the user enters a
   * train number to a train departure that has not left.
   * Does not exist in the Scan Class because of the attempt to retry or return to main menu.
   *
   * @param taskName is a string representing the task to be done to the train departure.
   * @return an existing train number that has not left.
   */
  private TrainDeparture getTrainDepNotLeft(String taskName) {
    try {
      int trainNumber = scan.getTrainNumberFromInterval();
      return trainRegister.getNotDepartedTrainFromTrainNumber(trainNumber, taskName);

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      System.out.println("Overview over train departures that has not departed can be found "
                             + "in main menu -> operation number 1.");
      if (scan.choiceRetry()) {
        return getTrainDepNotLeft(taskName);
      } else {
        returnToMainMenu();
        return null;
      }
    }
  }


  /**
   * Assigns a valid track number for a given train departure, ensuring a minimum of 10-minute
   * gap from other departures on the same track. If the track is not assigned, the method will
   * prompt for a track number to be set.If an invalid track is entered, the method prompts
   * the user to change the track until a valid track is entered.
   *
   * @param trainDeparture is the train departure the track assignment applies to.
   * @param track is the track that gets checked for validity.
   */
  private int setValidTrack(TrainDeparture trainDeparture, int track) {
    try {
      trainRegister.getTrainDepWithNewTrack(trainDeparture, track);
      return track;

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      track = scan.getTrackFromInterval();
      return setValidTrack(trainDeparture, track);
    }
  }


  /**
   * Registers a new train departure based on user input, and adds it to the register.
   * Validates each entered input, and continues to prompt for valid input as long as entered
   * input is invalid. Valid input has to be entered before proceeding to the next input
   * for registering a new train departure, and add it to the register.
   * If anything else goes wrong, the user can choose to retry og return to the main menu.
   * If new train departure was registered, the user must type '0' to return to the main menu.
   */
  private void newTrainDep() {
    try {
      LocalTime departureTime = scan.getAfterRefTime(trainRegister, "Departure time",
          trainRegister.getCurrentTime(), "current time");
      String line = scan.getValidLine();
      int trainNumber = getNonExistingTrainNumber();
      String destination = scan.getInputDestination();
      int track = getTrackFromChoice();

      TrainDeparture temporaryTrain = trainRegister.makeNewTrainDep(
          departureTime, line, trainNumber, destination, track);
      track = setValidTrack(temporaryTrain, track);
      TrainDeparture newTrainDep = trainRegister.getNewTrainRegistered(
          departureTime, line, trainNumber, destination, track);

      System.out.println("New train departure registered: ");
      System.out.println(trainRegister.writeOutTrainDep(newTrainDep));
      typeForReturn();

    } catch (Exception e) {
      System.out.println(e.getMessage());
      if (scan.choiceRetry()) {
        newTrainDep();
      } else {
        returnToMainMenu();
      }
    }
  }


  /**
   * Searches for a specific train departure with train number equal to the provided
   * train number from the user input, and prints it out.
   * If train departure was found, the user has to type '0' to return to the main menu.
   */
  private void searchByTrainNumber() {
    System.out.println("------------SEARCH TRAIN DEPARTURE BY TRAIN NUMBER------------");
    TrainDeparture trainDeparture = getExistingTrainDep();

    System.out.println("The train departure found: ");
    System.out.println(trainRegister.writeOutTrainDep(trainDeparture));
    typeForReturn();
  }


  /**
   * Searches for specific train departures with the destination equal to the provided
   * destination from the user input.
   * If no train departures were found, the user can choose to retry or return to the main menu.
   * If train departure(s) were found, the user has to type '0' to return to the main menu.
   */
  private void searchByDestination() {
    System.out.println("-------------SEARCH TRAIN DEPARTURES BY DESTINATION-------------");
    String destination = scan.getInputDestination();
    List<TrainDeparture> trainDepartures = trainRegister.getTrainDepByDestination(destination);

    if (trainDepartures.isEmpty()) {
      System.out.println("There are no train departures with " + destination
                             + " as final destination.");
      if (scan.choiceRetry()) {
        searchByDestination();
      } else {
        returnToMainMenu();
      }
    } else {
      System.out.println("Train departures with " + destination + " as final destination:");
      System.out.println(trainRegister.writeOutListTrainDep(trainDepartures));
      typeForReturn();
    }
  }


  /**
   * Searches for specific train departures with a track assigned equal to the provided
   * track number from the user input.
   * If no train departures departs from the inputted track, the user can choose to
   * retry or return to the main menu.
   * If train departure(s) were found, the user has to type '0' to return to the main menu.
   */
  private void searchByTrack() {
    System.out.println("----------------SEARCH TRAIN DEPARTURES BY TRACK----------------");
    int track = scan.getTrackFromInterval();
    List<TrainDeparture> trainDepartures = trainRegister.getTrainDepTimeByTrackSorted(track);

    if (trainDepartures.isEmpty()) {
      System.out.println("There are no future departure from track " + track + ".");
      if (scan.choiceRetry()) {
        searchByTrack();
      } else {
        returnToMainMenu();
      }
    } else {
      System.out.println("Train departures with departure from track number " + track + ":");
      System.out.println(trainRegister.writeOutListTrainDep(trainDepartures));
      typeForReturn();
    }
  }



  /**
   * Retrieves train departures with actual departure time within a specified time interval
   * from user input, and prints them out sorted by original departure time. After this, the user
   * has to type '0' to return to the main menu.
   * If no train departures were found within the time interval, the method prompts the user to
   * retry or return to the main menu.
   */
  private void searchInTimeInterval() {
    System.out.println("----------SEARCH TRAIN DEPARTURES WITHIN TIME INTERVAL----------");
    LocalTime fromTime = scan.getSameOrAfterRefTime(trainRegister, "From time",
        trainRegister.getCurrentTime(), "current time");
    LocalTime toTime = scan.getSameOrAfterRefTime(trainRegister, "To time", fromTime,
        "the start of the interval");

    List<TrainDeparture> trainDepartures =
        trainRegister.getTrainDepTimeIntervalSorted(fromTime, toTime);

    if (trainDepartures.isEmpty()) {
      System.out.println("There are currently no train departures with departure time from "
                             + fromTime + " to " + toTime + ".");
      if (scan.choiceRetry()) {
        searchInTimeInterval();
      } else {
        returnToMainMenu();
      }
    } else {
      System.out.println("Train departures with departures from "
                             + fromTime + " to " + toTime + ":");
      System.out.println(trainRegister.writeOutListTrainDep(trainDepartures));
      typeForReturn();
    }
  }

  /**
   * Prints out an information table.
   * Contains an information-table-header, a header for the train departures,
   * and the future train departure(s) listed downwards, sorted by original departure time.
   * If no train departures had original departure time at the current time, or after,
   * the method prints a message. The user has to type '0' to return back to the main menu.
   */
  private void printInformationTable() {
    System.out.println("-----------------------INFORMATION TABLE-----------------------");
    System.out.println("Current time of the day: " + trainRegister.getCurrentTime());
    System.out.println(" ");
    List<TrainDeparture> futureDepSorted = trainRegister.getFutureDeparturesSorted();

    if (futureDepSorted.isEmpty()) {
      System.out.println("There are currently no train departures from current time: "
                             + trainRegister.getCurrentTime()
                             + ". Please try again after registering a train departure.");
    } else {
      System.out.println(trainRegister.writeOutListTrainDep(futureDepSorted));
    }
    typeForReturn();
  }


  /**
   * Prints out the whole register og train departures, sorted by original departure time.
   * If there are no train departures in the register, the method prints a message indicating
   * no registered departures. The user has to type '0' to return back to the main menu.
   */
  private void printTrainRegisterSorted() {
    System.out.println("----------------ALL TRAIN DEPARTURES IN REGISTER----------------");
    System.out.println(trainRegister.writeOutRegister());
    typeForReturn();
  }


  /**
   * Updates the current time and registers it in the train register.
   * Prompts the user to enter the new current time, sets the current time as the new time,
   * and displays it. The user has to type '0' to return back to the main menu.
   * If entered input is before, or same, as the current time,
   * the user can type '0' to try again.
   */
  private void newCurrentTime() {
    try {
      System.out.println("--------------------------UPDATE CLOCK--------------------------");
      LocalTime inputNewTime = scan.promptTimeWithoutSeconds("New time (updated time to clock)");
      trainRegister.setCurrentTime(inputNewTime);
      System.out.println("Updated time registered for clock: " + inputNewTime + ".");
      typeForReturn();

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      if (scan.choiceRetry()) {
        newCurrentTime();
      } else {
        returnToMainMenu();
      }
    }
  }


  /**
   * Sets delay to a train departure from user input.
   * Prompts the user for an existing train departure, and a delay.
   * Prompts and updates the track for the train departure only if the delay causes a break of the
   * 10-minute gap with other train departures on the same track. Prints information about the
   * updated train departure. The user has to type '0' to return back to the main menu.
   */
  private void addDelay() {
    try {
      System.out.println("---------------------------SET DELAY---------------------------");
      TrainDeparture trainDeparture = getTrainDepNotLeft("set a delay");

      LocalTime delay = scan.promptTimeWithoutSeconds("Enter amount of delay");
      assert trainDeparture != null;
      trainDeparture = trainRegister.getTrainDepWithNewDelay(trainDeparture, delay);
      System.out.println(delay + " delay added to train departure.");

      setValidTrack(trainDeparture, trainDeparture.getTrack());

      System.out.println(trainRegister.writeOutTrainDep(trainDeparture));
      typeForReturn();

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      addDelay();
    }
  }


  /**
   * Assigns a track to an existing train departure.
   * Prompts the user for an existing train departure and a new track number. The method then
   * assigns the new track to the train departure, and displays the updated train departure.
   * The user has to type '0' to return back to the main menu. If exception is thrown, the user
   * can choose to retry og return to main menu.
   */
  private void assignTrack() {
    System.out.println("----------------------ASSIGN/CHANGE TRACK----------------------");
    TrainDeparture trainDeparture = getTrainDepNotLeft("assigned a track");

    int newTrack = scan.getTrackFromInterval();
    setValidTrack(trainDeparture, newTrack);

    System.out.println("Track successfully assigned:");
    System.out.println(trainRegister.writeOutTrainDep(trainDeparture));

    typeForReturn();
  }




  /**
   * Deletes a train departure with train number and confirmation from user input.
   * Prompts the user enter an existing train number, then prompts the user for
   * confirmation. Will continue to prompt until a valid input is entered.
   * If the user confirms deletion (enters 1), the chosen train departure is deleted.
   * If the user cancels deletion (enters 2), the chosen train departure is not deleted.
   * Handles exceptions if invalid input or errors occur during the deletion process.
   * The user has to type '0' to return back to the main menu.
   */
  private void deleteTrainDeparture() {
    try {
      System.out.println("---------------------DELETE TRAIN DEPARTURE---------------------");
      TrainDeparture trainDeparture = getTrainDepNotLeft("deleted");
      menu.showDeletionMenu(trainRegister.writeOutTrainDep(trainDeparture));

      if (scan.confirmChoiceOne()) {
        trainRegister.deleteTrainDep(trainDeparture);
        System.out.println("Train departure successfully deleted.");
      } else {
        System.out.println("Train departure not deleted.");
      }
      typeForReturn();

    } catch (Exception e) {
      System.out.println("Something went wrong:" + e.getMessage());
      if (scan.choiceRetry()) {
        deleteTrainDeparture();
      } else {
        returnToMainMenu();
      }
    }
  }


  /**
   * Starts the Train Dispatch Application by printing the main menu options.
   * Takes the entered input within a switch-case to fulfill the desired operation from the user.
   * When user decides to quit application, a corresponding message indicating that
   * the Train Dispatch App will exit is printed.
   */
  private void start() {
    menu.showMainMenu(trainRegister.getCurrentTime());
    while (!quitApplication) {
      String choice = scan.inputNotEmptyString();
      switch (choice.trim()) {
        case "1" -> printInformationTable();
        case "2" -> newCurrentTime();
        case "3" -> newTrainDep();
        case "4" -> deleteTrainDeparture();
        case "5" -> assignTrack();
        case "6" -> addDelay();
        case "7" -> searchByTrainNumber();
        case "8" -> searchByDestination();
        case "9" -> searchByTrack();
        case "10" -> searchInTimeInterval();
        case "11" -> printTrainRegisterSorted();
        case "12" -> quitApplication();
        default -> System.out.println("Registered entered choice: " + choice
                                          + ". Please enter a number from 1-12.");
      }
    }
    System.out.println("Exiting Train Dispatch App ...");
    System.out.println("Exit completed.");
    System.exit(0);
  }


  /**
   * Launches the application, by calling the method to pre-initialize train departures
   * and add them to the register, and calling the method for starting the application.
   */
  public void launch() {
    try {
      init();
      start();
    } catch (Exception e) {
      System.out.println(e.getMessage());
      returnToMainMenu();
    }
  }


}
