package edu.ntnu.stud.view;

import edu.ntnu.stud.model.TrainRegister;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;


/**
 * Scan Class.
 * Utility Class that uses Java Scanner to receive user input.
 * Help methods for the UserInterface Class to receive desired input.
 * Goal: Get input from the user of the application
 *
 * @author Hong An Ho
 * @version 1.2
 * @since 1.2
 */
public class Scan {
  private static final Scanner in = new Scanner(System.in);
  private static final String DESTINATION_REGEX = "^(?=.*[a-zA-ZÅÆØåæø])[a-zA-Z ÅÆØåæø-]+$";
  private static final String LINE_REGEX = "^[A-Z][1-9]\\d?$";
  private static final int MAX_TRAIN_NUMBER = 1000;
  private static final int MAX_TRACK_NUMBER = 20;


  /**
   * Constructs a new Scan instance.
   * Creates a new object of Scan without requiring any attribute initialization or set up.
   */
  public Scan() {
    //No initialization of attributes required to use the Scan class.
  }


  /**
   * Prompts the user to enter a non-empty string (including whitespaces).
   * If an empty string is entered, the method will print an error message and
   * continue to prompt for a non-empty string.
   *
   * @return a non-empty string from user input.
   */
  public String inputNotEmptyString() {
    String input = "";
    while (input.isEmpty()) {
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        input = scan.nextLine();
      }
      if (input.isEmpty()) {
        System.out.println("The registered string was empty. Please try again.");
      }
    }
    return input;
  }


  /**
   * Prompts the user to enter a line in the correct format (L1, A10 etc.).
   * If the entered input does not match the specific format for line, or is empty,
   * an error message will be printed out. The method will continue to prompt the user
   * for a correctly formatted line until the user enters a line in the correct format.
   *
   * @return line from user input in the correct format.
   */
  public String getValidLine() {
    System.out.println("Enter line for train departure:");
    String line = inputNotEmptyString();
    while (!line.matches(LINE_REGEX)) {
      System.out.println("The string for the line has to be a capital letter (A-Z) followed "
                             + "by a number from 1-99 "
                             + "( For example: 'A1', 'L20', etc.). Please try again.");
      line = getValidLine();
    }
    return line;
  }

  /**
   * Prompts the user to enter a valid destination.
   * If the entered input does not match the specific characters allowed in the destination name,
   * or is empty, an error message will be printed out. The method will continue to prompt the user
   * until the user enters a destination with only the allowed characters.
   *
   * @return destination from user input with valid characters.
   */
  public String getInputDestination() {
    System.out.println("Enter final destination for train departure:");
    String destination = inputNotEmptyString();
    while (!destination.matches(DESTINATION_REGEX.trim())) {
      System.out.println("The string for the destination has to contain at least one letter"
                             + ", and only letters from the norwegian alphabet (A-Å). "
                             + "Please try again.");
      destination = getInputDestination();
    }
    return destination;
  }


  /**
   * Prompts the user to enter a time that can be parsed to the index of a LocalTime.
   * If the entered input can not be parsed to the indexes of LocalTime, an error message
   * will be printed out, and the method will continue to prompt the user.
   *
   * @param parameterName is the name of the parameter of the time being prompted.
   *
   * @return a LocalTime with correct indexes.
   */
  public LocalTime promptTime(String parameterName) {
    System.out.println(parameterName + " (HH:MM):");
    try {
      LocalTime parameter = LocalTime.parse(in.next());
      in.nextLine();
      return parameter;

    } catch (DateTimeParseException e) {
      System.out.println(parameterName + " has to be in format HH:MM within a 24 hour range."
                             + " Please try again.");
      return promptTime(parameterName);
    }
  }


  /**
   * Prompts the user to enter a time in the format HH:MM.
   * If the entered input does not match the format HH:MM or HH:MM:SS where amount of seconds is
   * equal to 0, an error message will be printed out,
   * and the method will continue to prompt the user for a correctly formatted time.
   *
   * @param parameterName is the name of the parameter of the time being prompted.
   *
   * @return time from user input that can be written in the format HH:MM.
   */
  public LocalTime promptTimeWithoutSeconds(String parameterName) {
    LocalTime parameter = promptTime(parameterName);
    while (parameter.getSecond() != 0) {
      System.out.println(parameterName + " can only be in the format HH:MM:SS, if it can be "
                             + "converted unchanged to HH:MM.");
      parameter = promptTime(parameterName);
    }
    return parameter;
  }


  /**
   * Prompts the user to enter an integer.
   * If the entered input was not an integer, an error message will be printed. The method
   * will continue to prompt for an integer as long as a non-integer was entered.
   *
   * @param parameterName is the name of the parameter of the integer being prompted.
   *
   * @return integer from user input.
   */
  public int promptInt(String parameterName) {
    try {
      System.out.println(parameterName + ":");
      int parameter = in.nextInt();
      in.nextLine();
      return parameter;

    } catch (InputMismatchException e) {
      System.out.println("Wrong data type registered. Please enter an integer: ");
      in.nextLine();
      return promptInt(parameterName);
    }
  }


  /**
   * Prompts the user to enter a positive integer.
   * If the entered input is not a positive integer, the method will continue to prompt the
   * user until the entered input is a positive integer.
   *
   * @param parameterName is the name of the parameter of the integer being prompted.
   *
   * @return positive integer from user input.
   */
  public int getPosInt(String parameterName) {
    int parameter = promptInt(parameterName);
    while (parameter <= 0) {
      System.out.println("The registered input for " + parameterName
                             + " was not a positive integer. Please try again.");
      parameter = getPosInt(parameterName);
    }
    return parameter;
  }


  /**
   * Prompts the user to enter a train number from the train number interval (ranging from 1-999).
   * The method will continue to prompt the user until the user input
   * for train number is within this interval.
   *
   * @return train number from the train number interval.
   */
  public int getTrainNumberFromInterval() {
    int trainNumber = getPosInt("Train number for train departure");
    while (trainNumber >= MAX_TRAIN_NUMBER) {
      System.out.println("The registered input was " + trainNumber + ". The highest value a train "
                             + "number can have is 999. Please try again: ");
      trainNumber = getPosInt("Enter train number");
    }
    return trainNumber;
  }


  /**
   * Prompts the user to enter a track number from the track interval (ranging from 1-20).
   * The method will continue to prompt the user until the entered input
   * for track number is within this interval.
   *
   * @return track number from the track interval.
   */
  public int getTrackFromInterval() {
    int track = getPosInt("Track number for train departure");
    while (track > MAX_TRACK_NUMBER) {
      System.out.println("The registered input was " + track
                             + ". The track number must be between 1 and 20. Please try again.");
      track = getPosInt("Track");
    }
    return track;
  }


  /**
   * Prompts the user to enter a track number from the track interval (ranging from 1-20).
   * The method will continue to prompt the user until the entered input
   * for track number is within this interval.
   *
   * @return track number from the track interval.
   */
  public boolean confirmChoiceOne() {
    String choice = inputNotEmptyString().trim();
    while (!(choice.equals("1") || choice.equals("2"))) {
      System.out.println("Choose from 1 or 2. Please try again: ");
      choice = inputNotEmptyString().trim();
    }
    return choice.equals("1");
  }


  /**
   * Prompts the user to type '0' to retry after unsuccessful executing a method, or
   * anything else to return to the main menu.
   *
   * @return true if '0' was entered, and false if anything other than that was entered.
   */
  public boolean choiceRetry() {
    System.out.println("Type '0' to retry, "
                           + "or anything else to return to the main menu.");
    String choice = inputNotEmptyString();
    return choice.equalsIgnoreCase("0");
  }

  /**
   * Prompts the user to enter a time, after the referenced time.
   * If the entered input does not match the format HH:MM, or is before or equal to the
   * referenced, an error message will be printed out. The method will continue to
   * prompt the user until a correctly formatted time after the referenced time is entered.
   *
   * @param trainRegister is the register that gets used to check if the entered time is after the
   *                       referenced time.
   * @param parameterName is a LocalTime that gets checked if it is equal to the second time
   *                      parameter.
   * @param referencedTime is a LocalTime that the first time parameter compares with.
   * @param refTimeName is the name of the referenced time.
   * @return time in format HH:MM from user input in correct format, and after the referenced time.
   */
  public LocalTime getAfterRefTime(TrainRegister trainRegister, String parameterName,
                                    LocalTime referencedTime, String refTimeName) {
    try {
      LocalTime parameter = promptTimeWithoutSeconds(parameterName);
      trainRegister.verifyNotBeforeOrSameRefTime(parameter, referencedTime, refTimeName);
      return parameter;

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return getAfterRefTime(trainRegister, parameterName, referencedTime, refTimeName);
    }
  }


  /**
   * Retrieves a valid time input in the format HH:MM, which should be the same or
   * after the referenced. If the entered time format is incorrect or not within a 24-hour range,
   * the method prompts the user to retry or return to the main menu.
   *
   * @param trainRegister is the register that gets used to check if the entered time is after the
   *                       referenced time.
   * @param parameterName is the name of the parameter for which the time is being prompted.
   * @param referencedTime is the time the promoted time has to be equal too, or after.
   * @param refTimeName is the name of the referenced time.
   *
   * @return A time the same or after the referenced time, in the format HH:MM.
   */
  public LocalTime getSameOrAfterRefTime(TrainRegister trainRegister, String parameterName,
                                          LocalTime referencedTime, String refTimeName) {
    LocalTime time = promptTimeWithoutSeconds(parameterName);
    try {
      trainRegister.verifyNotBeforeReferencedTime(time, referencedTime, refTimeName);
      return time;

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return getSameOrAfterRefTime(trainRegister, parameterName, referencedTime, refTimeName);
    }
  }
}
