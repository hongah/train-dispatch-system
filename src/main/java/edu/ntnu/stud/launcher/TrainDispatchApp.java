package edu.ntnu.stud.launcher;

import edu.ntnu.stud.view.UserInterface;

/**
 * TrainDispatchApp Class.
 * This is the main class for the train dispatch application.
 * Initializes and launches the UserInterface.
 * Goal: Run Train Dispatch App Application.
 *
 * @author Hong An Ho
 * @version 1.0
 * @since 1.0
 */
public class TrainDispatchApp {
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();
    userInterface.launch();
  }
}
