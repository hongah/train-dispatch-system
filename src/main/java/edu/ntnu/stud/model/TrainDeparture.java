package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.util.Objects;


/**
 * TrainDeparture class.
 * Class describing a train departure.
 * Contains a constructor to create a train departure. The constructor verifies entered parameters,
 * and throws exceptions before a train departure is made.
 * Class has get-methods for every attribute belonging to a train departure, and set-methods
 * for the attributes that can be changed later.
 * Goal: act as a model of a train departure object.
 *
 * @author Hong An Ho
 * @version 1.1
 * @since 0.1
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private LocalTime delay;
  private static final String DESTINATION_REGEX = "^(?=.*[a-zA-ZÅÆØåæø])[a-zA-Z ÅÆØåæø-]+$";
  private static final String LINE_REGEX = "^[A-Z][1-9]\\d?$";


  /**
   * Verifies that the string parameter is not empty (including whitespaces).
   * The check fails if the string parameter is blank.
   *
   * @param parameter is the String that is to be verified.
   * @param parameterName is the name of the String being verified.
   * @throws IllegalArgumentException if the string is blank.
   */
  public void verifyStringParameterNotEmpty(String parameter, String parameterName)
      throws IllegalArgumentException {
    if (parameter.isBlank()) {
      throw new IllegalArgumentException("The string for the parameter '" + parameterName
                                             + "' was a blank string.");
    }
  }

  /**
   * Verifies that the String parameter for destination matches the regular expression
   * for destination.
   * The check fails if the parameter for destination does not match the valid format, or is empty.
   *
   * @param destination is the String for destination that is to be verified.
   * @throws IllegalArgumentException if the destination parameter does not match the valid format,
   *          or is empty.
   */
  private void verifyDestinationParameter(String destination) throws IllegalArgumentException {
    verifyStringParameterNotEmpty(destination, "destination");
    if (!destination.matches(DESTINATION_REGEX)) {
      throw new IllegalArgumentException("The string for the destination has to contain at least"
                                             + "one letter, and only letters from the norwegian "
                                             + "alphabet (A-Å).");
    }
  }


  /**
   * Verifies that the String parameter for line matches the regular expression for line.
   * The check fails if the parameter for line does not match the valid format, or is empty.
   *
   * @param line is the String for line that is to be verified.
   * @throws IllegalArgumentException if the line parameter does not match the valid format,
   *          or is empty.
   */
  private void verifyLineParameter(String line) throws IllegalArgumentException {
    verifyStringParameterNotEmpty(line, "line");
    if (!line.matches(LINE_REGEX)) {
      throw new IllegalArgumentException("The string for the line has to be a capital letter "
                                             + "(A-Z) followed by a number from 1-99 ( For example:"
                                             + " 'A1', 'L20', etc.).");
    }
  }


  /**
   * Verifies that the integer parameter is a positive number.
   * The check fails if the integer is not a positive number.
   *
   * @param parameter is the integer to be verified.
   * @param parameterName is a String of the name of the integer to be verified.
   * @throws IllegalArgumentException if the integer is either 0 or a negative number.
   */
  private void verifyPositiveInteger(int parameter, String parameterName)
      throws IllegalArgumentException {
    if (parameter <= 0) {
      throw new IllegalArgumentException("The value for the parameter '" + parameterName
                                             + "' was a negative integer.");
    }
  }


  /**
   * Verifies that the integer for train number is within the allowed interval
   * for train numbers (1-999).
   * The check fails if the integer for train number falls outside the allowed interval.
   *
   * @param trainNumber is the integer for train number to be verified.
   * @throws IllegalArgumentException if the train number is outside the allowed interval
   *          for train numbers.
   */
  private void verifyTrainNumberWithinInterval(int trainNumber) throws IllegalArgumentException {
    verifyPositiveInteger(trainNumber, "train number");
    if (trainNumber >= 1000) {
      throw new IllegalArgumentException("The highest value a train number can have is 999.");
    }
  }


  /**
   * Verifies that the integer for track number is within the allowed interval
   * for track numbers (1-20, but also -1 if not assigned yet).
   * The check fails if the integer for tack number falls outside the allowed interval.
   *
   * @param track is the integer for track number to be verified.
   * @throws IllegalArgumentException if the track number is outside the allowed interval
   *          for track numbers.
   */
  private void verifyTrackWithinInterval(int track) throws IllegalArgumentException {
    //track number sets to -1 if not assigned yet.
    if (track < -1 || track == 0 || track > 20) {
      throw new IllegalArgumentException("The track number has to be between 1 and 20.");
    }
  }


  /**
   * Verifies that the time can be converted to the right format HH:MM (or HH:MM:00).
   * The check fails if the time parameters is in the format HH:MM:SS, where seconds is not 00.
   *
   * @param time is the LocalTime for time to be verified.
   * @param parameterName is the name of the time to be verified.
   * @throws IllegalArgumentException if the time is not in the correct format.
   */
  public void verifyFormatTimeParameter(LocalTime time, String parameterName)
      throws IllegalArgumentException {
    //Time in this application can only be shown in format HH:MM.
    if (time.getSecond() != 0) {
      throw new IllegalArgumentException("The " + parameterName + " has to be in format HH:MM.");
    }
  }

  /**
   * Constructs a train departure by taking the parameters as attributes of the train departure.
   * The method verifies all the parameters first, before constructing a train departure.
   * Delay is automatically set to 00:00, which can be changed later.
   *
   * @param departureTime is a LocalTime representing the departure time of the train departure.
   * @param line is a big letter followed by an integer in a String which represents
   *              the train departure's route.
   * @param trainNumber is an integer representing a unique number of the specific train departure.
   * @param destination is a String representing the final destination of a train departure.
   * @param track is a LocalTime representing amount of time the train departure is delayed by.
   * @throws IllegalArgumentException if a String is blank, destination and line does not match
   *         the regular expression for destination and line, train number and track number is
   *         not within the specified interval, departure time is not in correct format.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber,
                        String destination, int track)
      throws IllegalArgumentException {

    verifyFormatTimeParameter(departureTime, "departure time");
    verifyLineParameter(line);
    verifyTrainNumberWithinInterval(trainNumber);
    verifyDestinationParameter(destination);
    verifyTrackWithinInterval(track);

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = LocalTime.MIN;
  }


  /**
   * Returns the LocalTime representing the departure time of an object of a train departure.
   *
   * @return departure time of the train departure.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }


  /**
   * Returns the String representing the line of an object of a train departure.
   *
   * @return line of the train departure.
   */
  public String getLine() {
    return line;
  }


  /**
   * Returns the integer representing the unique number for an object of a train departure.
   *
   * @return train number of the train departure.
   */
  public int getTrainNumber() {
    return trainNumber;
  }


  /**
   * Returns the String representing the final destination of an object of a train departure.
   *
   * @return destination of the train departure.
   */
  public String getDestination() {
    return destination;
  }


  /**
   * Returns the integer representing the track an object of a train departure departs from.
   *
   * @return track the train departure departs from.
   */
  public int getTrack() {
    return track;
  }


  /**
   * Returns the LocalTime representing the amount of delay of an object of train departure.
   *
   * @return amount of delay of the train departure.
   */
  public LocalTime getDelay() {
    return delay;
  }


  /**
   * Sets a track number to an object of train departure.
   * This method allows the track number of an object of train departure to be changed.
   *
   * @param newTrack is the track number to be set to the train departure.
   * @throws IllegalArgumentException if the track number to be set is outside the
   *        allowed track number interval.
   */
  public void setTrack(int newTrack) throws IllegalArgumentException {
    verifyTrackWithinInterval(newTrack);
    this.track = newTrack;
  }


  /**
   * Sets an amount of delay to an object of train departure.
   * This method allows the delay of an object of a train departure to be changed.
   *
   * @param newDelay is the delay to be set to the object of a train departure.
   * @throws IllegalArgumentException if the new delay is not in the right format.
   */
  public void setDelay(LocalTime newDelay) throws IllegalArgumentException {
    verifyFormatTimeParameter(newDelay, "new delay");
    this.delay = newDelay;
  }


  /**
   * Formats how an object of a train departure gets printed out, with all the train departure's
   * attributes, and returns it.
   * This method does not display the delay and track number, if delay is no delay (00:00) or
   * track number has not been assigned yet (track = -1).
   *
   * @return a formatted string with all information about the train departure.
   */
  @Override
  public String toString() {
    String stringDelay;
    String stringTrack;
    if (getDelay().equals(LocalTime.MIN)) {
      stringDelay = " ";
    } else {
      stringDelay = getDelay().toString();
    }
    if (getTrack() == -1) {
      stringTrack = " ";
    } else {
      stringTrack = Integer.toString(getTrack());
    }
    return String.format("%-17s%-6s%-15s%-15s%-8s%-5s",
        getDepartureTime(), getLine(), getTrainNumber(),
        getDestination(), stringDelay, stringTrack);
  }

  /**
   * Returns a formatted string representing the header when printing
   * one or multiple train departures.
   *
   * @return A formatted string with columns of headers for
   *           departure time, line, train number, destination, delay, and track.
   */
  public static String getHeader() {
    return String.format("%-17s%-6s%-15s%-15s%-8s%-5s",
        "Departure time",
        "Line",
        "Train number",
        "Destination",
        "Delay",
        "Track");
  }


  /**
   * Checks for equality between two train departures of the train departure objects,
   * based on the train departure's train number attribute.
   *
   * @param other is the other train departure object to be checked if is equal to this object.
   * @return true if the object is equal to this, and false otherwise.
   */
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    return trainNumber == ((TrainDeparture) (other)).trainNumber;
  }

  /**
   * Creates a hash code for the train departure object based on the identificator train number.
   * Items with the same train number will have the same hash code.
   *
   * @return a unique integer hash code that represents the item.
   */
  @Override
  public int hashCode() {
    return Objects.hash(trainNumber);
  }
}
