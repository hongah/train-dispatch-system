package edu.ntnu.stud.model;

import static edu.ntnu.stud.model.TrainDeparture.getHeader;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;



/**
 * TrainRegister class.
 * Handles all calculations for train departures and the clock.
 * Manages methods for manipulating, updating, adding and deleting train departures.
 * Goal: Manage train departures and related operations.
 *
 * @author Hong An Ho
 * @version 1.2
 * @since 0.2
 *
 */
public class TrainRegister {
  private final List<TrainDeparture> trainDepRegister;
  private LocalTime currentTime = LocalTime.MIN;


  /**
   * Constructs an object of the class TrainRegister with an empty ArrayList
   * that contains TrainDeparture objects from the TrainDeparture class.
   */
  public TrainRegister() {
    trainDepRegister = new ArrayList<>();
  }



  /**
   * Retrieves the current time of the day on the station.
   *
   * @return current time.
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }



  /**
   * Checks if a specific train departure exists in the register.
   *
   * @param trainDeparture the train departure that gets checked for existence.
   * @return true if train departure exist in the register, and false otherwise.
   */
  private boolean trainDepExist(TrainDeparture trainDeparture) {
    return trainDepRegister.contains(trainDeparture);
  }


  /**
   * Verifies that a specific train departure does not exist in the register.
   * The check fails if the train departure already exists in the register.
   *
   * @param trainDeparture the train departure to be verified for non-existence in the register.
   * @throws IllegalArgumentException if the provided train departure exists in the register.
   */
  private void verifyTrainDepNotExist(TrainDeparture trainDeparture)
      throws IllegalArgumentException {
    if (trainDepExist(trainDeparture)) {
      throw new IllegalArgumentException("The train number for the train departure is already "
                                         + "in use for another train departure in the register.");
    }
  }


  /**
   * Verifies that a specific train departure already exists in the register.
   * The check fails if the train departure does not exist in the register.
   *
   * @param trainDeparture the train departure to be verified for existence in the register.
   * @throws IllegalArgumentException if the provided train departure does not exist in register.
   */
  private void verifyTrainDepExist(TrainDeparture trainDeparture) throws IllegalArgumentException {
    if (!trainDepExist(trainDeparture)) {
      throw new IllegalArgumentException("The train departure does not exist in the register.");
    }
  }


  /**
   * Verifies that a specific train departure has not already departed from
   * the station (should not be able to modify delay or track, after train departure has departed).
   * The check fails if the train departure has departed.
   *
   * @param trainDeparture the train departure to be verified for non-departure.
   * @param taskName is the name of the task to be assigned to the train departure.
   * @throws IllegalArgumentException if the train departure has departed from the station.
   */
  private void verifyTrainDepNotLeft(TrainDeparture trainDeparture, String taskName)
      throws IllegalArgumentException {
    List<TrainDeparture> futureDepartures = getFutureDepartures();
    if (!futureDepartures.contains(trainDeparture)) {
      throw new IllegalArgumentException("The train departure with this train number has already "
                                           + "departed, and therefore can't be " + taskName + ".");
    }
  }


  /**
   * Makes a new train departure based on all the required parameters to create a train departure.
   * This method implements the default constructor of the TrainDeparture-class.
   *
   * @param departureTime is a LocalTime representing the original departure time of the train
   *                      departure.
   * @param line is a big letter followed by an integer in a String which represents
   *              the train departure's route.
   * @param trainNumber is an integer representing a unique number of the specific train departure.
   * @param destination is a String representing the final destination of a train departure.
   * @param track is a LocalTime representing amount of time the train departure is delayed by.
   * @return a new train departure.
   * @throws IllegalArgumentException if a String is blank, destination and line does not match
   *         the regular expression for destination and line, train number and track number is
   *         not within the specified interval, or if departure time is not in the correct format.
   */
  public TrainDeparture makeNewTrainDep(LocalTime departureTime, String line, int trainNumber,
                                        String destination, int track)
      throws IllegalArgumentException {
    return new TrainDeparture(departureTime, line, trainNumber, destination, track);
  }


  /**
   * Register a new train departure in the register.
   * Fails to add the train departure to the register if the train departure already
   * exist in the register, breaks the 10-minute-gap rule, or has departure time before
   * the current time of the day.
   *
   * @param departureTime is the departure time of the new train departure.
   * @param line is the line of the new train departure.
   * @param trainNumber is the train number of the new train departure.
   * @param destination is the final destination of the new train departure.
   * @param track is the track assigned to the new train departure.
   * @return the new registered train departure.
   * @throws IllegalArgumentException if invalid parameters are parsed in, or if train departure
   *          already exists in the register, or if train departure
   *          has less than a 10-minute gap from other train departures the same track, or has
   *          departure time before the current time of the day.
   */
  public TrainDeparture getNewTrainRegistered(LocalTime departureTime, String line, int trainNumber,
                                       String destination, int track)
      throws IllegalArgumentException {

    //verifies valid departure time before constructing a new train departure.
    verifyNotBeforeReferencedTime(departureTime, currentTime, "current time");

    TrainDeparture newTrainDeparture =
        makeNewTrainDep(departureTime, line, trainNumber, destination, track);

    verifyTrainDepNotExist(newTrainDeparture);
    verifyTrackForTrainDeparture(newTrainDeparture, track);

    trainDepRegister.add(newTrainDeparture);
    return newTrainDeparture;
  }


  /**
   * Retrieves an existing train departure or null, from the train number parameter.
   * Retrieves existing train departure if found, and null if not found.
   *
   * @param trainNumber is the unique integer which is checked if is equal to any
   *                    train departure's train number in the register.
   * @return train departure found from train number, or null if not found.
   */
  public TrainDeparture getTrainDepByTrainNumber(int trainNumber) {
    return trainDepRegister.stream()
               .filter(trainDeparture -> trainDeparture.getTrainNumber() == trainNumber)
               .findFirst()
               .orElse(null);
  }


  /**
   * Retrieves an existing train departure with the given train number.
   * Verifies that the train departure exist in the register.
   *
   * @param trainNumber is the train number of the train departure to be found.
   * @return existing train departure found from train number.
   * @throws IllegalArgumentException if the train departure already exist in the register.
   */
  public TrainDeparture getExistingTrainDepByTrainNumber(int trainNumber)
      throws IllegalArgumentException {
    TrainDeparture trainDeparture = getTrainDepByTrainNumber(trainNumber);
    verifyTrainDepExist(trainDeparture);
    return trainDeparture;
  }


  /**
   * Retrieves an existing train departure that has not departed yet, with the given train number.
   * Verifies that the train departure has not departed yet.
   *
   * @param trainNumber is the train number of the train departure to be found.
   * @param taskName is the name of the task to be assigned to the train departure.
   * @return existing train departure found from train number, that has not departed.
   * @throws IllegalArgumentException if the provided train departure exists in the register, or
   *          if the provided train departure has departed.
   */
  public TrainDeparture getNotDepartedTrainFromTrainNumber(int trainNumber, String taskName)
      throws IllegalArgumentException {
    TrainDeparture trainDeparture = getExistingTrainDepByTrainNumber(trainNumber);
    verifyTrainDepNotLeft(trainDeparture, taskName);
    return trainDeparture;
  }


  /**
   * Retrieves a list of train departures with destination equal to the given destination.
   * Retrieves an empty list if no train departures with the given destination is found.
   *
   * @param destination is a String representing the destination which is checked if is the
   *                    same as any train departure's destination in the register.
   * @return a list of train departures with the given destination, or an empty list.
   */
  public List<TrainDeparture> getTrainDepByDestination(String destination) {
    return trainDepRegister.stream().filter(
        trainDeparture -> trainDeparture.getDestination()
                              .equalsIgnoreCase(destination)).toList();
  }


  /**
   * Returns a boolean which represents if the given train departure's actual departure time
   * (departure time added by amount of time delayed) is within a given time interval that either
   * starts at, or after, the current time.
   * This method also counts actual departure time that is equal to the start-time of the interval,
   * or equal to the end-time of the interval, within the time interval.
   *
   * @param trainDeparture is the train departure to be checked is within the given time interval.
   * @param fromTime is a LocalTime which represents the start of the time interval.
   * @param toTime is a LocalTime which represents the end of the time interval.
   * @return true if the train departure is within the given time interval, and false it otherwise.
   */
  private boolean trainDepWithinTimeInterval(TrainDeparture trainDeparture, LocalTime fromTime,
                                       LocalTime toTime) {
    LocalTime depWithDelay =
        depWithDelay(trainDeparture.getDepartureTime(), trainDeparture.getDelay());

    boolean afterOrLikeFromTime =
        sameReferencedTime(depWithDelay, fromTime) || depWithDelay.isAfter(fromTime);
    boolean beforeOrLikeToTime =
        sameReferencedTime(depWithDelay, toTime) || beforeReferencedTime(depWithDelay, toTime);


    return afterOrLikeFromTime && beforeOrLikeToTime;
  }


  /**
   * Retrieves a list of train departures with actual departure time (departure time added by amount
   * of time delayed) within a given time interval that either starts at, or after, the current
   * time. Retrieves an empty list if no train departures within this time interval is found.
   *
   * @param fromTime is a LocalTime which represents the start of the time interval.
   * @param toTime is a LocalTime which represents the end of the time interval.
   * @return a list of train departures within the given time interval, or an empty list.
   */
  public List<TrainDeparture> getTrainDepWithinTimeInterval(LocalTime fromTime, LocalTime toTime) {
    return trainDepRegister.stream()
            .filter(trainDeparture -> trainDepWithinTimeInterval(trainDeparture, fromTime, toTime))
            .toList();
  }


  /**
   * Returns a boolean which represents if the first given time occurs before the second given time.
   *
   * @param checkTime is a LocalTime that gets checked is before the second time parameter.
   * @param referencedTime is a LocalTime that the first time parameter compares with.
   * @return true if the given time is before other given time, and false otherwise.
   */
  private boolean beforeReferencedTime(LocalTime checkTime, LocalTime referencedTime) {
    return checkTime.isBefore(referencedTime);
  }


  /**
   * Returns a boolean which represents if the first given time is equal to the second given time.
   *
   * @param checkTime is a LocalTime that gets checked if it is equal to the second time parameter.
   * @param referencedTime is a LocalTime that the first time parameter compares with.
   * @return true is the given times are equal, and false otherwise.
   */
  private boolean sameReferencedTime(LocalTime checkTime, LocalTime referencedTime) {
    return checkTime.equals(referencedTime);
  }


  /**
   * Verifies that the first given time is not before the referenced given time.
   * The check fails if the given time is before the referenced time.
   *
   * @param checkTime is a LocalTime that gets checked if it is before to the second time parameter.
   * @param referencedTime is a LocalTime that the first time parameter compares with.
   * @param refTimeName is the name of the referenced time.
   * @throws IllegalArgumentException if the first given time is before the referenced time.
   */
  public void verifyNotBeforeReferencedTime(
      LocalTime checkTime, LocalTime referencedTime, String refTimeName)
      throws IllegalArgumentException {
    if (beforeReferencedTime(checkTime, referencedTime)) {
      throw new IllegalArgumentException("The parsed time can't be before "
                                             + refTimeName + " " + referencedTime + ".");
    }
  }


  /**
   * Verifies that the first given time is not equal to the second given time.
   * The check fails if the first given time is equal to the referenced time.
   *
   * @param checkTime is a LocalTime that gets checked if it is equal to the second time parameter.
   * @param referencedTime is a LocalTime that the first time parameter compares with.
   * @param refTimeName is the name of the referenced time.
   * @throws IllegalArgumentException if the first given time is equal to the referenced time.
   */
  private void verifyNotSameTime(LocalTime checkTime, LocalTime referencedTime, String refTimeName)
      throws IllegalArgumentException {
    if (sameReferencedTime(checkTime, referencedTime)) {
      throw new IllegalArgumentException("The parsed time can't be the same as "
                                             + refTimeName + " " + referencedTime + ".");
    }
  }

  /**
   * Verifies that the first given time is not before or equal to the second given time.
   * The check fails if first given time is before or equal to the referenced time.
   *
   * @param checkTime is a LocalTime that gets checked if it is before or equal to
   *                  the second time parameter.
   * @param referencedTime is a LocalTime that the first time parameter compares with.
   * @param refTimeName is the name of the referenced time.
   * @throws IllegalArgumentException if the first given time is before or equal to
   *                  the referenced time.
   */
  public void verifyNotBeforeOrSameRefTime(LocalTime checkTime, LocalTime referencedTime,
                                           String refTimeName) throws IllegalArgumentException {
    verifyNotBeforeReferencedTime(checkTime, referencedTime, refTimeName);
    verifyNotSameTime(checkTime, referencedTime, refTimeName);
  }


  /**
   * Updates the clock on the station with the new given time update.
   * Verifies if the new updated time is before or after the current time before updating.
   * Fails if the new updated given time is before or equal to the current time.
   *
   * @param newTime is a LocalTime representing the new updated time to the clock.
   * @throws IllegalArgumentException if the new updated given time is before or equal to
   *                  the current time.
   */
  public void setCurrentTime(LocalTime newTime) throws IllegalArgumentException {
    verifyNotBeforeOrSameRefTime(newTime, currentTime, "current time");
    currentTime = newTime;
  }


  /**
   * Returns a train departure with a new delay set.
   * This method fails if the new delay is in valid format HH:MM.
   *
   * @param trainDeparture is the train departure to be given a new delay.
   * @param newDelay is the new delay that will be set to the train departure.
   * @return train departure with the new delay.
   * @throws IllegalArgumentException if the new delay is in invalid format.
   */
  public TrainDeparture getTrainDepWithNewDelay(TrainDeparture trainDeparture, LocalTime newDelay)
      throws IllegalArgumentException {
    trainDeparture.setDelay(newDelay);
    return trainDeparture;
  }


  /**
   * Calculates the actual departure time by adding delay to departure time.
   * This method does not change the departureTime-attribute of a train departure object.
   *
   * @param departureTime is a LocalTime representing the departure time for a train departure.
   * @param delay is a LocalTime representing the delay for a train departure.
   * @return the actual departure time of a train departure.
   */
  public LocalTime depWithDelay(LocalTime departureTime, LocalTime delay) {
    int hoursDelay = delay.getHour();
    int minutesDelay = delay.getMinute();
    return departureTime.plusHours(hoursDelay).plusMinutes(minutesDelay);
  }


  /**
   * Retrieves a list of future train departures with departure from the given track.
   * Retrieves an empty list if no future train departures with the given track is found.
   *
   * @param track is an integer which is checked if is the same as any
   *                    train departure's track in the register.
   * @return a list of train departures with the same track as the given track, or an empty list.
   */
  public List<TrainDeparture> getFutureTrainDepByTrack(int track) {
    List<TrainDeparture> futureTrainDep = getFutureDepartures();
    return futureTrainDep.stream().filter(
        trainDeparture -> (trainDeparture.getTrack() == track))
               .toList();
  }

  /**
   * Returns a boolean which represents if the first actual departure time is 10 minimum
   * minutes before another actual departure time.
   *
   * @param trainDepTimeWithDelay is a LocalTime representing the actual departure time of the
   *                              first train departure.
   * @param otherDepTimeWithDelay is a LocalTime representing the actual departure time of the other
   *                              train departure.
   * @param minMinApart is a LocalTime representing the minimum amount of time the train departure
   *                    has to be before the other train departure.
   * @return true if the first actual departure time is minimum 10 minutes before the other actual
   *          departure time, and false otherwise.
   */
  private boolean isTrain10MinBeforeOther(
      LocalTime trainDepTimeWithDelay, LocalTime otherDepTimeWithDelay, int minMinApart) {
    return beforeReferencedTime(
        trainDepTimeWithDelay, otherDepTimeWithDelay.plusMinutes(minMinApart));
  }


  /**
   * Returns a boolean which represents if the first actual departure time is minimum 10
   * minutes after another actual departure time.
   *
   * @param trainDepTimeWithDelay is a LocalTime representing the actual departure time of the
   *                              first train departure.
   * @param otherDepTimeWithDelay is a LocalTime representing the actual departure time of the other
   *                              train departure.
   * @param minMinApart is a LocalTime representing the minimum amount of time the train departure
   *                    has to be after the other train departure.
   * @return true if the first actual departure time is minimum 10 minutes
   *          after the other actual departure time, and false otherwise.
   */
  private boolean isTrain10MinAfterOther(
      LocalTime trainDepTimeWithDelay, LocalTime otherDepTimeWithDelay, int minMinApart) {
    return trainDepTimeWithDelay.isAfter(otherDepTimeWithDelay.minusMinutes(minMinApart));
  }



  /**
   * Returns a boolean which represents if there is a 10-minute gap (before and after)
   * between the actual departure time of a specific train departure and another train departure.
   *
   * @param trainDeparture is the train departure to be checked for the 10-minute gap.
   * @param otherTrain is the train departure that gets compared with.
   * @return true if there is a 10-minute-gap between the two departures, and false otherwise.
   */
  private boolean has10MinGapFromOther(TrainDeparture trainDeparture, TrainDeparture otherTrain) {
    int minMinApart = 10;

    LocalTime trainDepTimeWithDelay = depWithDelay(trainDeparture.getDepartureTime(),
        trainDeparture.getDelay());
    LocalTime otherDepTimeWithDelay = depWithDelay(otherTrain.getDepartureTime(),
        otherTrain.getDelay());

    return isTrain10MinBeforeOther(trainDepTimeWithDelay, otherDepTimeWithDelay, minMinApart)
               && isTrain10MinAfterOther(trainDepTimeWithDelay, otherDepTimeWithDelay, minMinApart);
  }


  /**
   * Returns a boolean which represents if the two given train departures are the same,
   * based by their train number by using equals method.
   *
   * @param trainDeparture is the train departure to be checked for.
   * @param otherTrain is the train departure that gets compared with.
   * @return true if the train departures are different, and false otherwise.
   */
  private boolean notSameTrain(TrainDeparture trainDeparture, TrainDeparture otherTrain) {
    return !trainDeparture.equals(otherTrain);
  }


  /**
   * Returns a boolean which represents if there is a 10-minute gap (before and after)
   * between the actual departure time of a specific train departure
   * and another train departures on the same track.
   *
   * @param trainDeparture is the train departure to be checked for.
   * @param track is the track the train departure wishes to have departure from.
   * @return true if the train departure has a minimum of 10-minute gap with other train
   *          departures from the same track, and false if not.
   */
  public boolean has10MinGapSameTrack(TrainDeparture trainDeparture, int track) {
    List<TrainDeparture> listSameTrack = getFutureTrainDepByTrack(track);

    return listSameTrack.stream().noneMatch(otherTrain -> (
                   notSameTrain(trainDeparture, otherTrain)
                   && has10MinGapFromOther(trainDeparture, otherTrain))
    );
  }


  /**
   * Returns a boolean which represents if there is a 10-minute gap (before and after)
   * between the actual departure time of a specific train departure
   * and another train departures on the same track.
   *
   * @param trainDeparture is the train departure to be checked for.
   * @param track is the track the train departure wishes to have departure from.
   * @return true if the train departure has a minimum of 10-minute gap with other train
   *          departures from the same track, and false if not.
   */
  private boolean validTrackToTrainDeparture(TrainDeparture trainDeparture, int track) {
    //Train departures with no assigned track, should freely set delay.
    if (track == -1) {
      return true;
    }
    return has10MinGapSameTrack(trainDeparture, track);
  }


  /**
   * Verifies that the given train departure can depart from the given track without conflicts.
   * The check fails if the train departure can not depart from the given track.
   *
   * @param trainDeparture is the train departure to be checked for.
   * @param track is the track the train departure wishes to be assigned.
   * @throws IllegalArgumentException if the train departure can not depart from the given track.
   */
  public void verifyTrackForTrainDeparture(TrainDeparture trainDeparture, int track)
      throws IllegalArgumentException {
    if (!validTrackToTrainDeparture(trainDeparture, track)) {
      throw new IllegalArgumentException("Train departure is within 10 minutes from another "
                                             + "train departure on the track. Please "
                                             + "change the track for this train departure.");
    }
  }


  /**
   * Retrieves the given train departure with the assigned given track.
   * Fails if the track violates the 10-minute gap rule.
   *
   * @param trainDeparture is the train departure that will get assigned the given track.
   * @param track is the track the train departure wishes to be assigned.
   * @return train departure with the new assigned given track.
   * @throws IllegalArgumentException if the train departure can not be assigned the given track,
   *        or if the track number is not within the allowed interval for track numbers.
   */
  public TrainDeparture getTrainDepWithNewTrack(TrainDeparture trainDeparture, int track)
      throws IllegalArgumentException {

    verifyTrackForTrainDeparture(trainDeparture, track);
    trainDeparture.setTrack(track);
    return trainDeparture;
  }


  /**
   * Retrieves a list of train departures that has been sorted
   * from earliest to latest by original departure time.
   * Retrieves an empty list if the given list is empty.
   *
   * @param trainDepartures is the list of train departures that will get sorted.
   * @return a list of train departures sorted by original departure time, or an empty list.
   */
  public List<TrainDeparture> getSortedByTime(List<TrainDeparture> trainDepartures) {
    return trainDepartures.stream().sorted(Comparator
               .comparing(TrainDeparture::getDepartureTime)).toList();
  }


  /**
   * Retrieves a list of train departures from the register,
   * that has actual departure time equal or after the current time.
   * Retrieves an empty list if there are no train departures in the register with actual departure
   * time after the current time.
   *
   * @return a list of train departures with actual departure time equal or after the current time,
   *           or empty list.
   */
  public List<TrainDeparture> getFutureDepartures() {
    return getTrainDepWithinTimeInterval(currentTime, LocalTime.MAX);
  }


  /**
   * Retrieves a list of future train departures, and returns them sorted
   * by original departure time.
   *
   * @return a list of future train departures sorted by original departure time.
   */
  public List<TrainDeparture> getFutureDeparturesSorted() {
    List<TrainDeparture> futureDepartures = getFutureDepartures();
    return getSortedByTime(futureDepartures);
  }


  /**
   * Retrieves a list of train departures within a specific time interval,
   * and returns them sorted by original departure time.
   *
   * @return a list of train departures within a specific time interval,
   *         sorted by original departure time.
   */
  public List<TrainDeparture> getTrainDepTimeIntervalSorted(LocalTime fromTime, LocalTime toTime) {
    List<TrainDeparture> trainDepTimeInterval = getTrainDepWithinTimeInterval(fromTime, toTime);
    return getSortedByTime(trainDepTimeInterval);
  }

  /**
   * Retrieves a list of train departures with departure from a specific track,
   * and returns them sorted by original departure time.
   *
   * @return a list of train departures with departure from specific track
   *         sorted by original departure time.
   */
  public List<TrainDeparture> getTrainDepTimeByTrackSorted(int track) {
    List<TrainDeparture> trainDepTimeInterval = getFutureTrainDepByTrack(track);
    return getSortedByTime(trainDepTimeInterval);
  }

  /**
   * Deletes the given train departure from the register.
   * Fails if the train departure does not exist in the register.
   *
   * @param trainDeparture is the train departure that will be removed from the register.
   *
   * @throws IllegalArgumentException if the train departure does not exist in the register.
   */
  public void deleteTrainDep(TrainDeparture trainDeparture) throws IllegalArgumentException {
    verifyTrainDepExist(trainDeparture);
    trainDepRegister.remove(trainDeparture);
  }



  /**
   * Returns a formatted String containing a header and train departures listed downwards.
   *
   * @param trainDepartures is the list of train departures that will get printed out.
   * @return A String starting with a header, followed by one and one train departure downwards.
   */
  public String writeOutListTrainDep(List<TrainDeparture> trainDepartures) {
    String header = getHeader();
    String allTrainDepartures = trainDepartures.stream().map(TrainDeparture::toString)
                                    .collect(Collectors.joining("\n"));
    return header + "\n" + allTrainDepartures;
  }

  /**
   * Returns a formatted String containing a header and the given train departure listed under.
   *
   * @param trainDeparture is the train departures that will get printed out.
   * @return A String starting with a header, followed by the given train departure.
   */
  public String writeOutTrainDep(TrainDeparture trainDeparture) {
    String header = getHeader();
    return header + "\n" + trainDeparture;
  }

  /**
   * Returns a formatted String containing a header and a list of train departures from the
   * register listed downwards.
   * If the register is empty, it returns a message indicating no registered departures.
   *
   * @return A String starting with a header and train departures from the register downwards,
   *          or a message indicating no registered departures.
   */
  public String writeOutRegister() {
    if (trainDepRegister.isEmpty()) {
      return "There are no train departures registered. "
                 + "Please try again after registering a train departure.";
    }
    return writeOutListTrainDep(getSortedByTime(trainDepRegister));
  }
}
